<?php

require_once('database.php');

class DB implements iDB
{
    private $link;
    private $queryResult;
    
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * 
     * @param string $query Zapytanie do wykonania
     * 
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);
        
        return (bool)$this->queryResult;
    }

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * 
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }
        
        return mysqli_fetch_assoc($this->queryResult);
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();
        
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        
        return $results;
    }
}

class Product
{

    private $db;

    public function __construct()
    {
        $this->db = new DB('localhost', 'root', '', 'iaimeetup');
    }

    public function checkProduct($product)
    {
        $this->db->query("SELECT * FROM `products`  WHERE `id` = '$product'");
        $products = $this->db->getAllRows();
        return $products;
    }

    public function addProduct($name, $price)
    {
        $this->db->query("INSERT INTO `products` SET `nazwa` = '$name', `price` = '$price'");  
    }
    public function removeProduct($product)
    {
        $this->db->query("DELETE FROM `products` WHERE `id` = '$product'");  
    }        
}

$options = array('uri' => 'http://localhost/');
$server = new SoapServer(NULL, $options);
$server->setClass('Product');
$server->handle();

$db = new DB('localhost', 'root', '', 'iaimeetup');

if (isset($_GET['action']))
{
    $product = $_GET['product'];
    $action = $_GET['action'];
    $name = $_GET['name'];
    $price = $_GET['price'];
    
    if ( $action == "checkProduct" )
    {
        $db->query("SELECT * FROM `products`  WHERE `id` = '$product'");
        $products = $db->getAllRows();
        //print_r(json_encode($products));

        header("Content-type: text/xml");
        $simplexml = new SimpleXmlElement('<?xml version="1.0"?><list/>');
        foreach($products as $item)
        {
            $simplexml->addChild('item', $item['nazwa']);
        }
        echo $simplexml->asXML();
    }
    elseif ( $action == "addProduct" )
    {
        $db->query("INSERT INTO `products` SET `nazwa` = '$name', `price` = '$price'");       
    }    
    elseif ( $action == "removeProduct" )
    {
        $db->query("DELETE FROM `products` WHERE `id` = '$product'");        
    }       
}
