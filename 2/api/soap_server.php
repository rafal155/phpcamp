<?php

class ServiceFunctions
{
	public function getInitials($firstName, $lastName)
	{
		$name = '';
		$name .= strtoupper($firstName, 0, 1);
		$name .= ' ' .strtoupper(substr($lastName, 0, 1));
		return $name;
	}
}

$options = array('uri' => 'http://localhost/');
$server = new SoapServer(NULL, $options);
$server->setClass('ServiceFunctions');
$server->handle();