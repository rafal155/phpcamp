<?php
//jakiś komentarz
require_once('database.php');

class DB implements iDB
{
    private $link;
    private $queryResult;
    
    /**
     * @param string $host Adres DB
     * @param string $login Login użytkownika
     * @param string $password Hasło użytkownika
     * @param string $dbName Nazwa bazy danych
     */
    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

    /**
     * Wykonuje zapytanie, zapisuje uchwyt
     * 
     * @param string $query Zapytanie do wykonania
     * 
     * @return bool Czy się udało wykonać zapytanie czy nie
     */
    public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);
        
        return (bool)$this->queryResult;
    }

    /**
     * Zwraca liczbę wierszy zmodyfikowanych przez ostatnie zapytanie
     * 
     * @return int
     */
    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Zwraca jeden (kolejny) wiersz z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }
        
        return mysqli_fetch_assoc($this->queryResult);
    }

    /**
     * Zwraca wszystkie wiersze z wyniku ostatniego zapytania
     * 
     * @return mixed
     */
    public function getAllRows()
    {
        $results = array();
        
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        
        return $results;
    }
}

$db = new DB('localhost', 'root', '', 'iaimeetup');
$db->query("SELECT * FROM `clients` LIMIT 10");

//var_dump($_POST);
/*if(!empty($_POST))
{

    $sql="INSERT INTO clients (id, name, surname, gender, date_of_birth, orders_count, street, city, postcode, country, notes).
    VALUES('', '$_POST[name]','$_POST[surname]','$_POST[gender]','$_POST[date_of_birth]','1','$_POST[street]','$_POST[city]','$_POST[postcode]','1','$_POST[notes]')";

   if ($db->query($sql))
  {
    echo 'Dodano do bazy';
  }
  else
  {
    echo 'Nie pykło';
  }         
}*/

//print $db->getAffectedRows();
//var_dump($db->getRow());
$clients = $db->getAllRows();

foreach($clients as $c)
{
  echo $c['name'];
}

?>


<table>
  <tbody>
    <tr>
      <td>data</td>
    </tr>
  </tbody>
</table>
